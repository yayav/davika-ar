﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class multipleButton : MonoBehaviour, IVirtualButtonEventHandler
{

    public VirtualButtonBehaviour[] virtualButtonBehaviors;
    string vbName;
    public GameObject slideShowUI, muteIcon;
    public Renderer slideMat;
    public AudioSource ambience;
    bool soundToggle = true;
    bool slideToggle = true;
    bool slideTrigger = false;
    public float changeTime = 10.0f;
    private float timeSinceLast = 1.0f;
    private int curSlide = 0;
    public Texture[] slides;

    void Start()
    {
        //sprite Material
        Renderer slideMat = GetComponent<Renderer>();

        //sound Toggle
        ambience = GetComponent<AudioSource>();

        //virtual button
        virtualButtonBehaviors = GetComponentsInChildren<VirtualButtonBehaviour>();
        for (int i = 0; i < virtualButtonBehaviors.Length; i++)
        {
            virtualButtonBehaviors[i].RegisterEventHandler(this);
        }
    }
    void Update()
    {
        //Debug.Log("Current slide Index: " + curSlide);
        //Debug.Log("+++++++++++++++++++++++++++++++++++++++++++ Current Time: " + timeSinceLast);
       // Debug.Log("Slide Trigger Bool: " + slideTrigger);
            if (timeSinceLast > changeTime && curSlide < slides.Length)
            {
                slideMat.material.SetTexture("_MainTex", slides[curSlide]);
                timeSinceLast = 0.0f;
                curSlide++;
             }
            else if (curSlide == slides.Length)
            {
                curSlide = 0;
            }
            timeSinceLast += Time.deltaTime;
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        vbName = vb.VirtualButtonName;
        if (vbName == "LogoButton")
        {
            if (slideToggle == true)
            {
                slideShowUI.SetActive(true);
            }
            else
            {
                slideShowUI.SetActive(false);
            }
        }
        else if (vbName == "SpeakerButton")
        {
            if (soundToggle == true)
            {
                muteIcon.SetActive(true);
                ambience.volume = 0.0f;
            }
            else
            {
                muteIcon.SetActive(false);
                ambience.volume = 0.3f;
            }
        }
    }
    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        vbName = vb.VirtualButtonName;
        if (vbName == "LogoButton")
        {
            slideToggle = !slideToggle;
            //slideTrigger = !slideTrigger;
        }
        else if (vbName == "SpeakerButton")
        {
            soundToggle = !soundToggle;
        }
    }
}
